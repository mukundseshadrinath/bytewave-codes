from numpy import array

#To input elements into the matrix
a=[[input() for i in range(3)] for j in range(3)]


#To print the user entered matrix
print "\n".join([str(b) for b in a])

#To check whether the elements are unique or not
y=[x for x in array(a).flat]
z=list(set(y))
if z!=y:
    print "\nOperation is not possible"
else :
    #Getting the output
    s=[[t[i] for t in a]for i in range(3)]
    x=[(i+1,j+1) for i in range(3) for j in range(3) if a[i][j]==max(s[j]) and a[i][j]==min(a[i])]
    for i in range(len(x)):
        print ("\n"+str(x[i][0])+"th row "+str(x[i][1])+"th column")
